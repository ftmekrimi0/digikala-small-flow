import './App.css'
 import React, { useState, useEffect } from "react"
 import Axios from "axios";
 import ProductCard from './Componenets/ProductCard'
 import ProductPage from './Componenets/ProductPage'
 import Pagenation from './Componenets/Pagenation'
 import { BrowserRouter as Router,Route,} from "react-router-dom"
function App() {

let content =null
  //Request Api 
  let config = {
    headers: {
      token :'mpfKW9ghVTCSuBZ7qTkSmEyvL38ShZxv',
    }
  } 
  const url = 'https://www.digikala.com/front-end/search/'
  const [products, setProducts] = useState({
    data : null,
  });
  useEffect( () => {
    Axios.get(url, config)
    .then(response =>{
      setProducts({
        data: response.data
      });
    });
  }, [url]);


 //Pagenation   
const [currentPage,setCurrentPage] = useState(1);
const [productperPage,setproductperPage] = useState(10);
const indexofLastproduct = currentPage * productperPage
const indexofFirstproduct = indexofLastproduct
const currentproduct = (indexofFirstproduct,indexofLastproduct )

const pagenate = (pageNumber) => setCurrentPage (pageNumber);

//Render Product
  if (products.data){
    content  = currentproduct.map(( product ) =>{
    <div key={products.id}>
      <ProductCard
      product ={ product} />
      </div>
    });
   
  }
  return (
  
    <div className="App">
           <header className="App-header">
           DigiKala small Flow
           </header>
           <main className="App-main"> 
            <Router>
          <Route exact path='/products/:id' component={ProductPage}>
          </Route>
            </Router> 
            <Pagenation
            productperpage={productperPage}
            totalproduct ={products.length} 
            pagenate={pagenate} />
           </main>
        </div>
     
  );
}

export default App;
