

function ProductPage(props) {
    return(

<div className=''>
      <div className=''>
        {props.product.images.main}
      </div>
    <div className=''>
     نام محصول : {props.product.title}
    </div>
    <div className=''>
     قیمت  <del> {props.product.price.selling_price}</del> 
     {props.product.price.rrp_price}
    </div>
   
   </div>

    )
}
export default ProductPage
