const Pagenation = ({productperpage,totalproduct , pagenate}) => {
    const Pagenumbers = []
    for(let i=1 ; i<= Math.ceil(totalproduct/productperpage); i++){
        Pagenumbers.push(i);
    }
     return (
         <nav>
             <ul className='Pagenation'>
                 {
                     Pagenumbers.map( number => (
                         <li key={number} className="page_item">
                             <a onClick ={() =>  pagenate(number)}>{number}</a>
                         </li>
                     ))
                 }
             </ul>
         </nav>
     )
}

export default Pagenation