import { Link  } from "react-router-dom";
function ProductCard(props) {
    return(

<div className='card__product_container'>
      <div className='card__product__img'>
        {props.product.images.main}
      </div>
    <div className='card__product__title'>
      {props.product.title}
    </div>
    <Link to={`/products/${props.product.id}`}>
        <button > View More </button>
   </Link> 
   </div>

    )
}
export default ProductCard
